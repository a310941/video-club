const express = require('express');
const controller = require('../controllers/users');
const router = express.Router();

/* GET users listing. */
router.post('/', controller.create);
router.get('/:page?', controller.list);
router.get('/show/:id', controller.index);
router.put('/:id', controller.edit);
router.patch('/:id', controller.replace);
router.delete('/:id', controller.remove);

module.exports = router;
