const express = require ('express');

function create (req,res,next){
    let email = req.body.email;
    let name = req.body.name;
    let lastName= req.body.lastName;
    let password=req.body.password;
    
    let user = new Object({
        email:email,
        name:name,
        lastName:lastName,
        password:password
    });

    console.log(user);
    res.json(user);
    //res.send(`Genera un nuevo usuario del sistema${user}`);
}

function list(req,res, next){
    let page = req.params.page ? req.params.page:0;
    res.send(`Mostrar todos los usuarios de la pagina ${page}`);
}

function index (req,res,next){
    const id =req.params.id;
    res.send(`Usuario # ${id}`);
}

function edit (req,res,create){
    const id =req.params.id;
    res.send(`Modificar al usuario # ${id}`);
}

function replace (req,res,next){
    const id =req.params.id;
    res.send(`Reemplazar al Usuario # ${id}`);
}

function remove (req,res,next){
    const id =req.params.id;
    res.send(`Remover al Usuario # ${id}`);
}

module.exports={
    create,list,index,edit,replace,remove
}